#!/bin/bash

if [ $# -lt 1 ]; then
		echo "No file given"
		exit 1
fi

if st-info --descr>/dev/null; then
		openocd -f interface/stlink-v2-1.cfg  -f target/stm32f4x.cfg -c "init; program $1 0x08000000 verify; reset run; exit"
		# openocd -f interface/stlink-v2-1.cfg  -f target/stm32f4x.cfg -c "init; reset halt; stm32f4x unlock 0; reset halt; exit"
		# st-flash reset
		# st-flash erase
		# st-flash --reset --serial `st-info --serial` write "$1" 0x08000000
fi
