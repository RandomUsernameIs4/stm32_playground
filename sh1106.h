#ifndef SH1106_H
#define SH1106_H

#include "stm32f4xx.h"

uint8_t SH1106Address;

uint8_t SH1106Set00Buffer[];
uint8_t SH1106OutBuffer[1029];

uint8_t SH1106InitBuffer[];

uint8_t charTable[256][8];
uint8_t* dotChar;
uint8_t* blankChar;
uint8_t* minusChar;

#endif
