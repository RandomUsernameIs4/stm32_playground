#include "sh1106.h"

uint8_t SH1106Address = 0b1111000;

uint8_t SH1106Set00Buffer[]={0x80,	//control byte
                           0xB0,//Page
                           0x00,//Low Column
                           0x10,//High Column
													 

};
uint8_t SH1106InitBuffer[]={
                          0x80,	//control byte
                          0xae,//Display OFF
                          0x00,//Low Column
                          0x10,//High Column
                          0xB0,//Page
                          0x40,//Start line
                          0xA1,//remap //potrzebne
                          0xDA,//com pins 
                          0x12,
                          0x20,
                          0x0,
                          0xD3, //display offset
                          0x00,//NO offset
                          0xc0,//scan direction
                          0xc8,
                          0xA6,//normal display
                          0xA4,//display ON
                          0x81,//set contrast
                          0x00,//contrast DATA
                          0xa8,//multiplex ratio 
                          0xff,//1/64 duty 
                          0xD5,//Display clock divide
                          0x00,
                          0xd9,//precharge period
                          0xF1,
                          0xDB,//VCOM deselect
                          0x40,
                          0x8d,//charge pump 
                          0x14,
                          0xAF,//display ON
};

uint8_t* dotChar=&charTable['.'][0];
uint8_t* blankChar=&charTable[0][0];
uint8_t* minusChar=&charTable['-'][0];

uint8_t charTable[][8]={{0,0,0,0,0,0,0,0}, //blankChar
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {0x0,0b00010000,0b00010000,0b00010000,0b00010000,0b00010000,0b00010000,0x0}, //minusChar
                        {0x0,0b0,0b0,0b01100000,0b01100000,0b0,0b0,0x0}, //dotChar
                        {},
                        {0x0,0b00111100,0b01100010,0b01010001,0b01001001,0b00100110,0b00011100,0x0}, //0
                        {0x0,0b00001000,0b00000100,0b00000010,0b01111111,0b00000000,0b00000000,0x0}, //1
                        {0x0,0b01100010,0b01110001,0b01010001,0b01010001,0b01011001,0b01001110,0x0}, //2
                        {0x0,0b00100010,0b01000001,0b01001001,0b01001001,0b01001001,0b00110110,0x0}, //3
                        {0x0,0b00011000,0b00010100,0b00010010,0b00010001,0b01111111,0b00010000,0x0}, //4
                        {0x0,0b00100111,0b01000101,0b01000101,0b01000101,0b01000101,0b00111001,0x0}, //5
                        {0x0,0b00111110,0b01001001,0b01001001,0b01001001,0b01001001,0b00110010,0x0}, //6
                        {0x0,0b00000001,0b00000001,0b01110001,0b00001001,0b00000101,0b00000011,0x0}, //7
                        {0x0,0b00110110,0b01001001,0b01001001,0b01001001,0b01001001,0b00110110,0x0}, //8
                        {0x0,0b00100110,0b01001001,0b01001001,0b01001001,0b01001001,0b00110110,0x0}  //9
};
