#ifndef DS18B20
#define DS18B20

#include "onewire.h"

volatile uint8_t temperature;
volatile uint8_t temperatureRemainder;
volatile uint8_t belowZero;

void MeasureTemperature(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN);

int TemperatureToString(char string[]);

#endif
