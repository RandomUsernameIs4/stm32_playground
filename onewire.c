#include "onewire.h"

void Delay(TIM_TypeDef* TIM, uint32_t delay)
{
	TIM->CNT=0;
	TIM->EGR |= TIM_EGR_UG;
	TIM->CR1 |= TIM_CR1_CEN; // counter enabled
	while(TIM->CNT<delay);
}

uint8_t Initialize(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN)
{
	uint32_t shift = 0;
	while(PIN>>shift)shift++;
	uint8_t PRESENCE=0;
	GPIO->MODER |= 1<<shift;
	GPIO->ODR &= ~PIN; //ustawienie stanu niskiego
	
	Delay(TIM, 480); //odczekanie 480us
	GPIO->ODR |= PIN; //odpuszczenie do stanu wysokiego
	GPIO->MODER &= ~(0b11<<shift);

	Delay(TIM, 15); //odczekanie 15us na reakcje czujnika

	//sprawdzenie odpowiedzi
	TIM->CNT=0;
	TIM->EGR |= TIM_EGR_UG;
	TIM->CR1 |= TIM_CR1_CEN; // counter enabled
	while(TIM->CNT<300)
	{
		if(!(GPIO->IDR & PIN))
			PRESENCE=1;
	}

	return PRESENCE;
}

void Send1(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN)
{
	uint32_t shift = 0;
	while(PIN>>shift)shift++;
	GPIO->MODER |= 1<<shift;
	GPIO->ODR &= ~PIN;
	Delay(TIM, 1);
	GPIO->ODR |= PIN;
	Delay(TIM, 61);
}
void Send0(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN)
{
	uint32_t shift = 0;
	while(PIN>>shift)shift++;
	GPIO->MODER |= 1<<shift;
	GPIO->ODR &= ~PIN;
	Delay(TIM, 60);
	GPIO->ODR |= PIN;
	Delay(TIM, 1);
}

uint32_t Read(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN)
{
	uint32_t shift = 0;
	while(PIN>>shift)shift++;
	GPIO->MODER |= 1<<shift;
	GPIO->ODR &= ~PIN;
	Delay(TIM, 1);
	GPIO->ODR |= PIN;
	GPIO->MODER &= ~(0b11<<shift);

	while(TIM->CNT<14)
		if(!(GPIO->IDR & PIN))
		{
			Delay(TIM, 60-TIM->CNT);
			return 0;
		}

	Delay(TIM, 46);
	return 1;
}

void SendCommand(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN, uint32_t command)
{
	for(int i=0; i<8; i++)
	{
		if(command & (1<<i))
			Send1(TIM, GPIO, PIN);
		else
			Send0(TIM, GPIO, PIN);
	}
}
