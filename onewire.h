#ifndef ONEWIRE_H
#define ONEWIRE_H

#include "stm32f4xx.h"

void Delay(TIM_TypeDef* TIM, uint32_t delay);

uint8_t Initialize(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN);

/* All write time slots must be a minimum of 60μs in duration with a minimum of a 1μs recovery */
/* time between individual write slots. Both types of write time slots are initiated by the master pulling the */
/* 1-Wire bus low (see Figure 14). */
/* To generate a Write 1 time slot, after pulling the 1-Wire bus low, the bus master must release the 1-Wire */
/* bus within 15μs. When the bus is released, the 5kΩ pullup resistor will pull the bus high. To generate a */
/* Write 0 time slot, after pulling the 1-Wire bus low, the bus master must continue to hold the bus low for */
/* the duration of the time slot (at least 60μs). */
void Send1(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN);
void Send0(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN);

/* All read time slots must be a minimum of 60μs in duration with a minimum of a 1μs recovery time */
/* between slots. A read time slot is initiated by the master device pulling the 1-Wire bus low for a */
/* minimum of 1μs and then releasing the bus (see Figure 14). After the master initiates the read time slot, */
/* the DS18B20 will begin transmitting a 1 or 0 on bus. The DS18B20 transmits a 1 by leaving the bus high */
/* and transmits a 0 by pulling the bus low. When transmitting a 0, the DS18B20 will release the bus by the */
/* end of the time slot, and the bus will be pulled back to its high idle state by the pullup resister. Output */
/* data from the DS18B20 is valid for 15μs after the falling edge that initiated the read time slot. Therefore, */
/* the master must release the bus and then sample the bus state within 15μs from the start of the slot. */
uint32_t Read(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN);

void SendCommand(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN, uint32_t command);

#endif
