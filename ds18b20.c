#include "ds18b20.h"
#include "sh1106.h"

void MeasureTemperature(TIM_TypeDef* TIM, GPIO_TypeDef* GPIO, uint32_t PIN)
{
	if(!Initialize(TIM, GPIO, PIN))
	{
		ErrorLed();
		return;
	}
	SendCommand(TIM, GPIO, PIN, 0xcc); //skip rom
	SendCommand(TIM, GPIO, PIN, 0x44); //convert t
	while(Read(TIM, GPIO, PIN)==0);

	if(!Initialize(TIM, GPIO, PIN))
	{
		ErrorLed();
		return;
	}
	SendCommand(TIM, GPIO, PIN, 0xcc); //skip rom
	SendCommand(TIM, GPIO, PIN, 0xbe); //read scratchpad
	uint16_t response=0;
	int i=0;
	for(i=0; i<10; i++)
		response |= (Read(TIM, GPIO, PIN)<<i);
	
	belowZero = (Read(TIM, GPIO, PIN)==1);
	
	//The master may issue a reset to terminate reading at any time if only part of the scratchpad data is needed.
	Initialize(TIM, GPIO, PIN);

	temperatureRemainder=response&0b1111;
	response>>=4; //loose precision
	temperature=response;
}

int TemperatureToString(char string[])
{
	uint8_t temp = temperature;
	int i=0, lasti=0;
	for(;temp>0;i++)
		temp/=10;

	if(belowZero)
	{
		string[0]='-';
		i++;
		lasti=1;
	}
	
	temp = temperature;
	for(int k=i-1;k>=lasti;k--)
	{
		string[k] = '0'+temp%10;
		temp/=10;
	}

	string[i++] = '.';

	temp = (temperatureRemainder*625)/100;
	for(int k=i+1;k>=i;k--)
	{
		string[k] = '0'+temp%10;
		temp/=10;
	}
	return i+2;
}
