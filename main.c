#include "stm32f4xx.h"
#include "sh1106.h"
#include "BMP280/bmp280.h"

#include <string.h>

volatile uint8_t measureTemperature=0;
volatile uint8_t refreshDisplay=0;

volatile int8_t response=0;
struct bmp280_dev bmp280;
struct bmp280_config bmp280Conf;
struct bmp280_uncomp_data bmp280UcompData;
double temp, press;
int8_t SPIWrite(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length);
int8_t SPIRead(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length);
void SPIDelay(uint32_t period);

uint8_t I2CSlaveAddress;

void ErrorLed(int set)
{
	if(set)
		GPIOC->BSRR = GPIO_BSRR_BS3;
	else
		GPIOC->BSRR = GPIO_BSRR_BR3;
}

void SystemClock_Config(void)
{
	/* Enable Power Control clock */
	// RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	// PWR->CR = PWR_CR_VOS;
	/* The voltage scaling allows optimizing the power consumption when the device is
	   clocked below the maximum system frequency, to update the voltage scaling value
	   regarding system frequency refer to product datasheet.  */
	//__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

	RCC->CR |= (RCC_CR_HSION);
	uint32_t timeout = 3;
	while(!(RCC->CR & RCC_CR_HSIRDY))
	{

		if(timeout == 0)
			ErrorLed(1);
		else
			timeout--;
	}
	
	//https://stm32f4-discovery.net/2015/01/properly-set-clock-speed-stm32f4xx-devices/
	RCC->PLLCFGR |=
		(RCC_PLLCFGR_PLLSRC_HSI |
		 //The software has to set these bits correctly to ensure that the VCO input frequency ranges
		 //from 1 to 2 MHz. It is recommended to select a frequency of 2 MHz to limit PLL jitter.
		 //VCO input frequency = PLL input clock frequency / PLLM with 2 ≤ PLLM ≤ 63
		 //2 = 16MHz / 8
		 RCC_PLLCFGR_PLLM_3 |
		 //The software has to set these bits correctly to ensure that the VCO output frequency is
		 //between 100 and 432 MHz. (check also Section 6.3.20: RCC PLLI2S configuration register
		 //(RCC_PLLI2SCFGR))
		 //VCO output frequency = VCO input frequency × PLLN with 50 ≤ PLLN ≤ 432
		 //400 = 2 * 200
		 (200<< RCC_PLLCFGR_PLLN_Pos) |
		 //Set and cleared by software to control the frequency of the general PLL output clock.
		 //These bits can be written only if PLL is disabled.
		 //Caution: The software has to set these bits correctly not to exceed 100 MHz on this domain.
		 //PLL output clock frequency = VCO frequency / PLLP with PLLP = 2, 4, 6, or 8
		 //00: PLLP = 2
		 //01: PLLP = 4
		 //10: PLLP = 6
		 //11: PLLP = 8
		 //100 = 400/4
		 RCC_PLLCFGR_PLLP_0 |
		 //Main PLL (PLL) division factor for USB OTG FS, and SDIO clocks Set and cleared by software
		 //to control the frequency of USB OTG FS clock, and the SDIOclock. These bits should be
		 //written only if PLL is disabled.
		 //Caution: The USB OTG FS requires a 48 MHz clock to work correctly. The SDIO need a
		 //frequency lower than or equal to 48 MHz to work correctly.
		 //USB OTG FS clock frequency = VCO frequency / PLLQ with 2 ≤ PLLQ ≤ 15
		 //45 = 400/9
		 (9 << RCC_PLLCFGR_PLLQ_Pos));
	
	                 
	RCC->CR |= RCC_CR_PLLON;
	timeout = 2;
	while(!(RCC->CR & RCC_CR_PLLRDY))
	{
		if(timeout == 0)
			ErrorLed(1);
		else
			timeout--;
	}
	
	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
	   clocks dividers */
	RCC->CFGR |=
		(//Set and cleared by software to select the system clock source.
		 RCC_CFGR_SW_PLL |
		 //AHB prescaler
		 //Set and cleared by software to control AHB clock division factor.
		 //Caution: The clocks are divided with the new prescaler factor from 1 to 16 AHB cycles after HPRE write.
		 RCC_CFGR_HPRE_DIV1 |
		 //APB Low speed prescaler (APB1)
		 //Set and cleared by software to control APB low-speed clock division factor.
		 //Caution: The software has to set these bits correctly not to exceed 50 MHz on this domain.
		 //The clocks are divided with the new prescaler factor from 1 to 16 AHB cycles after PPRE1
		 //write.
		 RCC_CFGR_PPRE1_DIV2 |
		 //APB high-speed prescaler (APB2)
		 //Set and cleared by software to control APB high-speed clock division factor.
		 //Caution: The software has to set these bits correctly not to exceed 100 MHz on this domain.
		 //The clocks are divided with the new prescaler factor from 1 to 16 AHB cycles after PPRE2 write.	   
		 RCC_CFGR_PPRE2_DIV1
		 );
	
	SysTick->LOAD = 0;                         /* set reload register */
	SysTick->VAL = 0;                                             /* Load the SysTick Counter Value */
	SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk |
		SysTick_CTRL_TICKINT_Msk   |
		SysTick_CTRL_ENABLE_Msk;                         /* Enable SysTick IRQ and SysTick Timer */
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;	
}

void WWDG_IRQHandler()
{
	ErrorLed(1);
}
void UsageFault_Handler()
{
	ErrorLed(1);
}

void DisplayOnLeds(uint32_t number)
{
	GPIOC->BSRR = (GPIO_BSRR_BR13 |
	               GPIO_BSRR_BR12 |
	               GPIO_BSRR_BR11 |
	               GPIO_BSRR_BR10 |
	               GPIO_BSRR_BR9 |
	               GPIO_BSRR_BR8 |
	               GPIO_BSRR_BR7 |
	               GPIO_BSRR_BR6 |
	               GPIO_BSRR_BR5);
	GPIOC->BSRR = (number<<GPIO_BSRR_BS5_Pos);	
}

void SendI2C(uint8_t address, uint8_t* buffer, int count)
{
	while(I2C1->SR2 & I2C_SR2_BUSY);
	
	GPIOC->BSRR = GPIO_BSRR_BS2;

	I2CSlaveAddress = address;
	DMA1_Stream7->M0AR = buffer;
	DMA1_Stream7->NDTR = count;
	I2C1->CR1 |= I2C_CR1_START;
}

void DisplayOnLCD(char string[])
{
	SendI2C(SH1106Address, &SH1106Set00Buffer, 4);

	SH1106OutBuffer[0] = 0x40;

	int i=0, j=0;
	for(;string[i]!='\0';i++)
	{
		if(string[i]=='\n')
		{
			while((j%16)!=0)
			{
				memcpy(&SH1106OutBuffer[8*j+1], charTable['\0'], 8);
				j++;
			}
		}
		else
		{
			memcpy(&SH1106OutBuffer[8*j+1], charTable[string[i]], 8);
			j++;
		}
	}
	
	for(;j<128;j++)
		memcpy(&SH1106OutBuffer[8*j+1], charTable['\0'], 8);

	SendI2C(SH1106Address, &SH1106OutBuffer, 1024+1);
}

int IntToString(int input, char string[])
{
	if(input==0)
	{
		string[0]='0';
		return 1;
	}
	
	int tmp = input;
	int len=0;
	for(;tmp>0;len++)
		tmp/=10;

	int firstIndex = 0;
	if(input<0)
	{
		string[0] = '-';
		len++;
		firstIndex = 1;
	}
	
	tmp = input;
	for(int k=len-1; k>=firstIndex; k--)
	{
		string[k] = '0'+tmp%10;
		tmp /= 10;
	}

	return len;
}

int DoubleToString(double input, char string[], int precision)
{
	if(input==0)
	{
		string[0]='0';
		return 1;
	}
	
	double tmp = input;

	int multiplier=1;
	while(tmp>=10)
	{
		multiplier*=10;
		tmp/=10;
	}

	int len=0;
	if(input<0)
		string[len++] = '-';
	
	tmp = input;
	while(multiplier >= 1)
	{
		string[len++] = '0'+((int)(tmp/multiplier)%10);
		multiplier/=10;
	}
	
	string[len++] = '.';

	tmp = input-(int)input;
	for(int k=0; k<precision;k++)
	{
		tmp *= 10;
		string[len++] = '0'+((int)tmp)%10;
		tmp -= (int)(tmp);
	}

	return len;
}

void TIM4_IRQHandler()
{
	if (TIM4->SR & TIM_SR_UIF)
	{
		if(GPIOC->ODR & GPIO_ODR_OD4)
			GPIOC->BSRR = GPIO_BSRR_BR4;
		else
			GPIOC->BSRR = GPIO_BSRR_BS4;
		TIM4->SR ^= TIM_SR_UIF; // reset the status register
	}
}

void TIM3_IRQHandler()
{
	if (TIM3->SR & TIM_SR_UIF)
	{
		measureTemperature = 1;

		TIM3->SR ^= TIM_SR_UIF; // reset the status register
	}
}

void TIM5_IRQHandler()
{
	if (TIM5->SR & TIM_SR_UIF)
	{
		refreshDisplay = 1;

		TIM5->SR ^= TIM_SR_UIF; // reset the status register
	}
}

int main(void)
{
	SystemClock_Config();

	//leds
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	
	GPIOC->MODER |=
		(GPIO_MODER_MODE2_0 |
		 GPIO_MODER_MODE3_0 |
		 GPIO_MODER_MODE4_0 |
		 GPIO_MODER_MODE5_0 |
		 GPIO_MODER_MODE6_0 |
		 GPIO_MODER_MODE7_0 |
		 GPIO_MODER_MODE8_0 |
		 GPIO_MODER_MODE9_0 |
		 GPIO_MODER_MODE10_0 |
		 GPIO_MODER_MODE11_0 |
		 GPIO_MODER_MODE12_0 |
		 GPIO_MODER_MODE13_0
		 );

	GPIOC->OTYPER &=
		~(GPIO_OTYPER_OT2 |
		  GPIO_OTYPER_OT3 |
		  GPIO_OTYPER_OT4 |
		  GPIO_OTYPER_OT5 |
		  GPIO_OTYPER_OT6 |
		  GPIO_OTYPER_OT7 |
		  GPIO_OTYPER_OT8 |
		  GPIO_OTYPER_OT9 |
		  GPIO_OTYPER_OT10 |
		  GPIO_OTYPER_OT11 |
		  GPIO_OTYPER_OT12 |
		  GPIO_OTYPER_OT13);

	GPIOC->PUPDR |=
		(GPIO_PUPDR_PUPD2_1 |
		 GPIO_PUPDR_PUPD3_1 |
		 GPIO_PUPDR_PUPD4_1 |
		 GPIO_PUPDR_PUPD5_1 |
		 GPIO_PUPDR_PUPD6_1 |
		 GPIO_PUPDR_PUPD7_1 |
		 GPIO_PUPDR_PUPD8_1 |
		 GPIO_PUPDR_PUPD9_1 |
		 GPIO_PUPDR_PUPD10_1 |
		 GPIO_PUPDR_PUPD11_1 |
		 GPIO_PUPDR_PUPD12_1 |
		 GPIO_PUPDR_PUPD13_1); //pull down

	NVIC_EnableIRQ(TIM4_IRQn);
	NVIC_SetPriority(TIM4_IRQn, 0x1e);
	TIM4->DIER |= TIM_DIER_UIE; // enable update interrupt
	TIM4->PSC = 49;
	TIM4->ARR = 1000; // count to 1 (autoreload value 1)
	TIM4->EGR |= TIM_EGR_UG;
	TIM4->CR1 |= TIM_CR1_CEN;
	
	//spi
	RCC->APB1ENR |= (RCC_APB1ENR_TIM2EN | RCC_APB1ENR_TIM3EN);
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
	RCC->AHB1ENR |= ( RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOAEN );

	NVIC_EnableIRQ(TIM3_IRQn);
	NVIC_SetPriority(TIM3_IRQn, 0x1e);
	TIM3->DIER |= TIM_DIER_UIE; // enable update interrupt
	TIM3->PSC = 49;
	TIM3->ARR = 125000; // 125ms
	TIM3->CR1 |= TIM_CR1_CEN; // counter enabled

	TIM2->CR1 |= TIM_CR1_OPM; //Counter stops counting at the next update event (clearing the bit CEN)
	TIM2->PSC = 49999; //1ms tick (50MHz/(49999+1) = 1kHz)
	TIM2->ARR = 1000; // count to 1s (autoreload value 1)
	
	//af5, afrl
	GPIOB->AFR[0] |= ( GPIO_AFRL_AFSEL3_2 | GPIO_AFRL_AFSEL3_0 |
										 GPIO_AFRL_AFSEL5_2 | GPIO_AFRL_AFSEL5_0 );
	GPIOA->AFR[0] |= ( GPIO_AFRL_AFSEL6_2 | GPIO_AFRL_AFSEL6_0 );
	//B3 SCK
	//A6 MISO
	//B5 MOSI
	//B6 SS
	GPIOA->MODER |= ( GPIO_MODER_MODE6_1 );
	GPIOB->MODER |= ( GPIO_MODER_MODE3_1 |
	                  GPIO_MODER_MODE5_1 |
										GPIO_MODER_MODE6_0 ); //alternate, 6 output

	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPD6_Msk ); // no pull
	GPIOB->PUPDR |= GPIO_PUPDR_PUPD3_1;
	
	GPIOB->OSPEEDR |= ( 0b11<<GPIO_OSPEEDR_OSPEED3_Pos
	                    | 0b11<<GPIO_OSPEEDR_OSPEED4_Pos
	                    | 0b11<<GPIO_OSPEEDR_OSPEED5_Pos
											);

	SPI1->CR1 &= ~SPI_CR1_SPE;
	SPI1->CR1 |= ( 0b1<<SPI_CR1_BR_Pos //fPCLK/4 = 100Mhz/4, żeby analizator nadążył
								 //cpol i CPHA jest 00 domyślnie
								 //SPI_CR1_DFF jest 0 domyślnie
								 //MSB first (0)
								 | SPI_CR1_MSTR 
								 );

	SPI1->CR2 |= SPI_CR2_SSOE;
	SPI1->CR1 |= SPI_CR1_SPE;
	
	//i2c+dma
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;
	
	DMA1_Stream7->CR &= ~DMA_SxCR_EN;
	NVIC_EnableIRQ(DMA1_Stream7_IRQn);
	NVIC_SetPriority(DMA1_Stream7_IRQn, 0x2);
	DMA1_Stream7->CR |= (1<<DMA_SxCR_CHSEL_Pos |
	                     1<<DMA_SxCR_DIR_Pos |
	                     DMA_SxCR_TCIE |
	                     DMA_SxCR_MINC |
	                     0<<DMA_SxCR_PL_Pos);
	DMA1_Stream7->PAR = &I2C1->DR;

	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
	//i2c: _2=4 (AF4) AFR[1], bo 8 i 9 to wyższy rejestr
	GPIOB->AFR[1] |= ( GPIO_AFRH_AFSEL8_2 | GPIO_AFRH_AFSEL9_2 );
	GPIOB->MODER |= ( GPIO_MODER_MODE8_1 | GPIO_MODER_MODE9_1); //alternate
	GPIOB->OTYPER |= (GPIO_OTYPER_OT8 | GPIO_OTYPER_OT9); //open drain
	GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPD8_Msk | GPIO_PUPDR_PUPD9_Msk); //no pull

	I2C1->CR1 &= ~I2C_CR1_PE;
	NVIC_SetPriority(I2C1_EV_IRQn, 0x2);
	NVIC_EnableIRQ(I2C1_EV_IRQn);

	I2C1->CR1 |= I2C_CR1_ACK;
	I2C1->CR2 |= (50<<I2C_CR2_FREQ_Pos); //50MHz
	I2C1->CR2 |= (I2C_CR2_ITEVTEN); //event interupt enabled


	I2C1->OAR1 &= ~(I2C_OAR1_ADDMODE_Msk); //7-bit
	I2C1->CCR |= (1<<I2C_CCR_FS_Pos |
	              1<<I2C_CCR_DUTY_Pos|
	              //freq = 50Mhz -> t = 20 ns
	              //T high = 9 * CCR * T PCLK1
	              //T low = 16 * CCR * T PCLK1
	              //Th+Tl=1/400kHz = 1250 ns
	              //CCR * 20 * (9+16) = 1250 ns -> CCR = 2.5
	              2<<I2C_CCR_CCR_Pos);

	//maximum allowed SCL rise time in fm: 300
	//freq = 50Mhz -> t = 20 ns -> 300/20 = 15
	//15+1=16
	I2C1->TRISE = 16<<I2C_TRISE_TRISE_Pos;

	I2C1->CR1 |= I2C_CR1_PE;

	RCC->APB1ENR |= (RCC_APB1ENR_TIM5EN);
	NVIC_SetPriority(TIM5_IRQn, 0x1f);
	TIM5->DIER |= TIM_DIER_UIE; // enable update interrupt
	TIM5->PSC = 49;
	TIM5->ARR = 1000; // 1000000 to 1 seknuda
	TIM5->CR1 |= TIM_CR1_CEN; // counter enabled
	NVIC_EnableIRQ(TIM5_IRQn);	
	
	//------------------------------------------------------

	bmp280.dev_id = 0;
	bmp280.read = SPIRead;
	bmp280.write = SPIWrite;
	bmp280.delay_ms = SPIDelay;
	bmp280.intf = BMP280_SPI_INTF;
	
	response = bmp280_init(&bmp280);
	if(response != BMP280_OK)
		ErrorLed(1);

	/* Always read the current settings before writing, especially when
	 * all the configuration is not modified
	 */
	response = bmp280_get_config(&bmp280Conf, &bmp280);
	if(response != BMP280_OK)
		ErrorLed(1);

	/* configuring the temperature oversampling, filter coefficient and output data rate */
	/* Overwrite the desired settings */
	bmp280Conf.filter = BMP280_FILTER_COEFF_2;

	/* Temperature oversampling set at 4x */
	bmp280Conf.os_temp = BMP280_OS_4X;

	/* Pressure over sampling none (disabling pressure measurement) */
	bmp280Conf.os_pres = BMP280_OS_4X;

	/* Setting the output data rate as 1HZ(1000ms) */
	bmp280Conf.odr= BMP280_ODR_62_5_MS;
	response = bmp280_set_config(&bmp280Conf, &bmp280);
	if(response != BMP280_OK)
		ErrorLed(1);

	/* Always set the power mode after setting the configuration */
	response = bmp280_set_power_mode(BMP280_NORMAL_MODE, &bmp280);
	if(response != BMP280_OK)
		ErrorLed(1);
	
	SendI2C(SH1106Address, &SH1106InitBuffer, 30);

	while(1)
	{
		if(measureTemperature > 0 && !(SPI1->SR & SPI_SR_BSY))
		{
			ErrorLed(0);
			response = bmp280_get_uncomp_data(&bmp280UcompData, &bmp280);
			if(response != BMP280_OK)
				ErrorLed(1);
			
			measureTemperature = 0;
		}
		if(refreshDisplay > 0 && !(I2C1->SR2 & I2C_SR2_BUSY) && !(SPI1->SR & SPI_SR_BSY))
		{
			response = bmp280_get_comp_temp_double(&temp, bmp280UcompData.uncomp_temp, &bmp280);
			if(response != BMP280_OK)
				ErrorLed(1);
			response = bmp280_get_comp_pres_double(&press, bmp280UcompData.uncomp_press, &bmp280);
			if(response != BMP280_OK)
				ErrorLed(1);
			
			char string[128];
			string[127]='\0';
			int len = DoubleToString(temp, string, 2);
			string[len++]='\n';
			len += DoubleToString(press/100, &string[len], 2);
			string[len+1]='\0';
			DisplayOnLCD(string);
			refreshDisplay = 0;
		}
	}
}

void I2C1_EV_IRQHandler()
{
	if(I2C1->SR1 & I2C_SR1_SB)
		I2C1->DR = I2CSlaveAddress;

	if(I2C1->SR1 & I2C_SR1_ADDR || I2C1->SR1 & I2C_SR1_ADD10)
	{
		volatile int temp = I2C1->SR2;
		DMA1_Stream7->CR |= DMA_SxCR_EN;
		I2C1->CR2 |= I2C_CR2_DMAEN;
	}
	if(I2C1->SR1 & I2C_SR1_BTF)
	{
		if(DMA1_Stream7->NDTR == 0)
		{
			I2C1->CR1 |= I2C_CR1_STOP;
			GPIOC->BSRR = GPIO_BSRR_BR2;
		}
	}
}

void DMA1_Stream7_IRQHandler()
{
	//transfer complete
	if(DMA1->HISR & DMA_HISR_TCIF7)
	{
		I2C1->CR2 &= ~I2C_CR2_DMAEN;
		DMA1_Stream7->CR &= ~DMA_SxCR_EN;
		I2C1->CR2 |= I2C_CR2_LAST;
		DMA1->HIFCR |= DMA_HIFCR_CTCIF7;
	}

	//half transfer
	if(DMA1->HISR & DMA_HISR_HTIF7)
	{
		DMA1->HIFCR |= DMA_HIFCR_CHTIF7;
	}

	//transfer error
	if(DMA1->HISR & DMA_HISR_TEIF7)
	{
		ErrorLed(1);
		DMA1->HIFCR |= DMA_HIFCR_CTEIF7;
	}
	//fifo error
	if(DMA1->HISR & DMA_HISR_FEIF7)
	{
		ErrorLed(1);
		DMA1->HIFCR |= DMA_HIFCR_CFEIF7;
	}
}

int8_t SPIWrite(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length)
{
	GPIOB->BSRR |= GPIO_BSRR_BR6;
	while(! (SPI1->SR & SPI_SR_TXE));
	SPI1->DR = reg_addr;
	while(! (SPI1->SR & SPI_SR_RXNE));
	uint32_t rsp = SPI1->DR;
	while(! (SPI1->SR & SPI_SR_TXE));
	for(int i=0;i<length;i++)
	{
		SPI1->DR = reg_data[i];
		while(! (SPI1->SR & SPI_SR_RXNE));
		rsp = SPI1->DR;
		while(! (SPI1->SR & SPI_SR_TXE));
	}
	while(SPI1->SR & SPI_SR_BSY);
	GPIOB->BSRR |= GPIO_BSRR_BS6;
	
	return 0;
}

int8_t SPIRead(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length)
{
	GPIOB->BSRR |= GPIO_BSRR_BR6;
	while(! (SPI1->SR & SPI_SR_TXE));
	SPI1->DR = reg_addr;
	while(! (SPI1->SR & SPI_SR_RXNE));
	uint32_t rsp = SPI1->DR;
	while(! (SPI1->SR & SPI_SR_TXE));
	for(int i=0;i<length;i++)
	{
		SPI1->DR = 0;
		while(! (SPI1->SR & SPI_SR_RXNE));
		reg_data[i] = SPI1->DR;
		while(! (SPI1->SR & SPI_SR_TXE));
	}
	while(SPI1->SR & SPI_SR_BSY);
	GPIOB->BSRR |= GPIO_BSRR_BS6;
	
	return 0;
}

void SPIDelay(uint32_t period)
{
	TIM2->CNT=0;
	TIM2->EGR |= TIM_EGR_UG;
	TIM2->CR1 |= TIM_CR1_CEN; // counter enabled
	while(TIM2->CNT<period);	
}
